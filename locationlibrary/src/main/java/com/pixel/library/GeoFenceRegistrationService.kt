@file:Suppress("PrivatePropertyName")

package com.pixel.library

import android.app.IntentService
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.Geofence.GEOFENCE_TRANSITION_ENTER
import com.google.android.gms.location.GeofenceStatusCodes
import com.google.android.gms.location.GeofencingEvent

@Suppress("VariableNaming")
class GeoFenceRegistrationService(name: String = "GeoIntentService") : IntentService(name) {

    private val TAG = name
    var icon = R.drawable.ic_baseline_location_on_24

    override fun onHandleIntent(intent: Intent?) {
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        val geoFenceId = intent?.getStringExtra("GEO_FENCE_ID")
        val iconId = intent?.getIntExtra("GEO_FENCE_NOTIFICATION_ICON", -1)
        if (iconId != null && icon > 0) {
            icon = iconId
        }
        if (geofencingEvent.hasError()) {
            Log.d(TAG, "GeoFencingEvent error " + geofencingEvent.errorCode)
        } else {
            val transaction = geofencingEvent.geofenceTransition
            val geofences = geofencingEvent.triggeringGeofences
            val geofence = geofences[0]
            if (transaction == GEOFENCE_TRANSITION_ENTER && geofence.requestId == geoFenceId) {
                Log.d(TAG, "You are inside $geoFenceId")
            } else {
                Log.d(TAG, "You are outside $geoFenceId")
            }
            val geofenceTransitionDetails: String =
                getGeofenceTransitionDetails(transaction, geofences)
            sendNotification(geofenceTransitionDetails)
        }
    }

    // Create a detail message with Geofences received
    private fun getGeofenceTransitionDetails(
        geoFenceTransition: Int,
        triggeringGeofences: List<Geofence>
    ): String {
        // get the ID of each geofence triggered
        val triggeringGeofencesList: ArrayList<String> = ArrayList()
        for (geofence in triggeringGeofences) {
            triggeringGeofencesList.add(geofence.requestId)
        }
        var status: String? = null
        if (geoFenceTransition == GEOFENCE_TRANSITION_ENTER) status =
            "Entering " else if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) status =
            "Exiting "
        return status + TextUtils.join(", ", triggeringGeofencesList)
    }

    // Send a notification
    private fun sendNotification(msg: String) {
        Log.d(TAG, "sendNotification: $msg")

        // Intent to start the main Activity
        val notificationIntent = Intent(this, this::class.java)
        val stackBuilder: TaskStackBuilder = TaskStackBuilder.create(this)
        stackBuilder.addParentStack(this::class.java)
        stackBuilder.addNextIntent(notificationIntent)
        val notificationPendingIntent: PendingIntent? =
            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)

        // Creating and sending Notification
        val notificationMng = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationMng.notify(
            0,
            createNotification(msg, notificationPendingIntent)
        )
    }

    // Create a notification
    private fun createNotification(
        msg: String,
        notificationPendingIntent: PendingIntent?
    ): Notification {
        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this)
        notificationBuilder
            .setSmallIcon(icon)
            .setColor(Color.RED)
            .setContentTitle(msg)
            .setContentText("Geofence Notification!")
            .setContentIntent(notificationPendingIntent)
            .setDefaults(Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE or Notification.DEFAULT_SOUND)
            .setAutoCancel(true)
        return notificationBuilder.build()
    }

    // Handle errors
    private fun getErrorString(errorCode: Int): String {
        return when (errorCode) {
            GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE -> "GeoFence not available"
            GeofenceStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES -> "Too many GeoFences"
            GeofenceStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS -> "Too many pending intents"
            else -> "Unknown error."
        }
    }
}
