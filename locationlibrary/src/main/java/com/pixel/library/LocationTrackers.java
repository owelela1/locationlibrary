package com.pixel.library;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Address;
import android.location.Location;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import io.reactivex.Observable;

public class LocationTrackers {

    private LocationTracker mLocationTracker = new LocationTracker();


    public Observable<Location> lastKnownLocation(Context context) {
        return mLocationTracker.lastKnownLocation(context);
    }

    public Observable<ActivityRecognitionResult> activityLocation(Context context, int detectIntervalMilliseconds) {
        return mLocationTracker.activityLocation(context, detectIntervalMilliseconds);
    }

    public Observable<Location> updatesLocation(Context context, int priority,
                                                int numUpdates, Long intervalMilliseconds) {
        return mLocationTracker.updatesLocation(context, priority, numUpdates, intervalMilliseconds);
    }

    public Observable<List<Address>> geocodeLocation(Context context,
                                                     String locationName, int maxResults) {
        return mLocationTracker.geocodeLocation(context, locationName, maxResults);
    }

    public Observable<List<Address>> geocodeReverseLocation(Context context, double lat, double lng,
                                                            int maxResults) {
        return mLocationTracker.geocodeReverseLocation(context, lat, lng, maxResults);
    }

    public PendingIntent getGeoFencePendingIntent(Context context,
                                                  PendingIntent pendingIntent,
                                                  String geoFenceId,
                                                  int geoFenceNotificationIcon) {
        return mLocationTracker.getGeoFencePendingIntent(context, pendingIntent, geoFenceId, geoFenceNotificationIcon);
    }

    public Observable<Status> startGeoFencing(Context context, PendingIntent pendingIntentSet,
                                              int geoFenceNotificationIcon, Long durationMillis,
                                              LatLng latLng, Float radius, int notificationResponsivenessMs,
                                              int loiteringDelayMs, String geoFenceId) {
        return mLocationTracker.startGeoFencing(context, pendingIntentSet, geoFenceNotificationIcon,
                durationMillis, latLng, radius, notificationResponsivenessMs, loiteringDelayMs, geoFenceId);
    }

    public Observable<Status> stopGeoFencing(Context context, PendingIntent pendingIntentSet,
                                             LatLng latLng, String geoFenceId, int geoFenceNotificationIcon) {
        return mLocationTracker.stopGeoFencing(context, pendingIntentSet, latLng, geoFenceId, geoFenceNotificationIcon);
    }

    public Geofence getGeoFence(Long durationMillis, LatLng latLng, Float radius,
                                int notificationResponsivenessMs, int loiteringDelayMs, String geoFenceId) {
        return mLocationTracker.getGeoFence(durationMillis, latLng, radius, notificationResponsivenessMs,
                loiteringDelayMs, geoFenceId);
    }

    public GeofencingRequest geoFenceRequest(Long durationMillis, LatLng latLng, Float radius,
                                             int notificationResponsivenessMs, int loiteringDelayMs,
                                             String geoFenceId) {
        return mLocationTracker.geoFenceRequest(durationMillis, latLng, radius, notificationResponsivenessMs,
                loiteringDelayMs, geoFenceId);
    }
}
