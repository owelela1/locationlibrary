package com.pixel.library

import android.Manifest
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Location
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.ActivityRecognitionResult
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Observable
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider

class LocationTracker {

    fun lastKnownLocation(context: Context): Observable<Location>? {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return null
        }

        val locationProvider = ReactiveLocationProvider(context)
        return locationProvider.lastKnownLocation
    }

    fun activityLocation(
        context: Context,
        detectIntervalMilliseconds: Int
    ): Observable<ActivityRecognitionResult>? {

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return null
        }

        val locationProvider = ReactiveLocationProvider(context)

        return locationProvider.getDetectedActivity(detectIntervalMilliseconds)
    }

    fun updatesLocation(
        context: Context,
        priority: Int,
        numUpdates: Int,
        intervalMilliseconds: Long
    ): Observable<Location>? {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return null
        }
        val request = LocationRequest.create() // standard GMS LocationRequest
            .setPriority(priority)
            .setNumUpdates(numUpdates)
            .setInterval(intervalMilliseconds)

        val locationProvider = ReactiveLocationProvider(context)
        return locationProvider.getUpdatedLocation(request)
    }

    fun geocodeLocation(
        context: Context,
        locationName: String,
        maxResults: Int
    ): Observable<List<Address>>? {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return null
        }

        val locationProvider = ReactiveLocationProvider(context)
        return locationProvider.getGeocodeObservable(locationName, maxResults)
    }

    fun geocodeReverseLocation(
        context: Context,
        lat: Double,
        lng: Double,
        maxResults: Int
    ): Observable<List<Address>>? {

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return null
        }

        val locationProvider = ReactiveLocationProvider(context)
        return locationProvider
            .getReverseGeocodeObservable(lat, lng, maxResults)
    }

    fun getGeoFencePendingIntent(
        context: Context,
        pendingIntent: PendingIntent?,
        geoFenceId: String,
        geoFenceNotificationIcon: Int?
    ): PendingIntent? {
        if (pendingIntent != null) {
            return pendingIntent
        }
        val intent = Intent(context, GeoFenceRegistrationService::class.java)
        intent.putExtra("GEO_FENCE_ID", geoFenceId)
        intent.putExtra("GEO_FENCE_NOTIFICATION_ICON", geoFenceNotificationIcon)
        return PendingIntent
            .getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    @Suppress("LongParameterList")
    fun startGeoFencing(
        context: Context,
        pendingIntentSet: PendingIntent?,
        geoFenceNotificationIcon: Int?,
        durationMillis: Long,
        latLng: LatLng,
        radius: Float,
        notificationResponsivenessMs: Int,
        loiteringDelayMs: Int,
        geoFenceId: String
    ): Observable<Status>? {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return null
        }

        val pendingIntent = getGeoFencePendingIntent(
            context,
            pendingIntentSet,
            geoFenceId,
            geoFenceNotificationIcon
        )

        val locationProvider = ReactiveLocationProvider(context)

        return locationProvider.addGeofences(
            pendingIntent,
            geoFenceRequest(
                durationMillis,
                latLng,
                radius,
                notificationResponsivenessMs,
                loiteringDelayMs,
                geoFenceId
            )
        )
    }

    fun stopGeoFencing(
        context: Context,
        pendingIntentSet: PendingIntent?,
        latLng: LatLng,
        geoFenceId: String,
        geoFenceNotificationIcon: Int?
    ): Observable<Status>? {

        val pendingIntent = getGeoFencePendingIntent(
            context,
            pendingIntentSet,
            geoFenceId,
            geoFenceNotificationIcon
        )

        val locationProvider = ReactiveLocationProvider(context)

        return locationProvider.removeGeofences(pendingIntent)
    }

    @Suppress("LongParameterList")
    @NonNull
    fun getGeoFence(
        durationMillis: Long,
        latLng: LatLng,
        radius: Float,
        notificationResponsivenessMs: Int,
        loiteringDelayMs: Int,
        geoFenceId: String
    ): Geofence {
        return Geofence.Builder()
            .setRequestId(geoFenceId)
            .setExpirationDuration(durationMillis)
            .setCircularRegion(
                latLng.latitude,
                latLng.longitude,
                radius
            )
            .setNotificationResponsiveness(notificationResponsivenessMs)
            .setLoiteringDelay(loiteringDelayMs)
            .setTransitionTypes(
                Geofence.GEOFENCE_TRANSITION_ENTER
                        or Geofence.GEOFENCE_TRANSITION_EXIT
                        or Geofence.GEOFENCE_TRANSITION_DWELL
            )
            .build()
    }

    @Suppress("LongParameterList")
    fun geoFenceRequest(
        durationMillis: Long,
        latLng: LatLng,
        radius: Float,
        notificationResponsivenessMs: Int,
        loiteringDelayMs: Int,
        geoFenceId: String
    ): GeofencingRequest {
        return GeofencingRequest.Builder()
            .setInitialTrigger(Geofence.GEOFENCE_TRANSITION_ENTER)
            .addGeofence(
                getGeoFence(
                    durationMillis,
                    latLng,
                    radius,
                    notificationResponsivenessMs,
                    loiteringDelayMs,
                    geoFenceId
                )
            )
            .build()
    }
}
